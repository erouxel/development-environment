{ pkgs, ... }:

{
  home.packages = with pkgs; [
    chromium
    fping
    ipmitool
    keepassxc
    less
    libsForQt5.okular
    openssl
    pavucontrol
    sqitchPg
    tree
    unzip
  ];

  home.file.".inputrc".text = ''
    "\e[A": history-search-backward
    "\e[B": history-search-forward
  '';

  programs.bash = {
    enable = true;
  };

  programs.vim = {
    enable = true;
    plugins = [
      pkgs.vimPlugins.solarized
    ];
    settings = {
      tabstop = 2;
      number = true;
    };
    extraConfig = ''
      set encoding=utf-8
      set hlsearch

      " Colors
      set t_Co=256
      colorscheme solarized
      let g:solarized_termcolors=256
      set background=dark
    '';
  };

  programs.htop = {
    enable = true;
  };

  programs.ssh = {
    enable = true;
    # matchBlocks = {
    #   "gitlab.com" = {
    #     identityFile = "~/.ssh/%h/id_ed25519";
    #   };
    # };
  };

  programs.gpg = {
    enable = true;
  };

  programs.firefox = {
    enable = true;
  };

  programs.git = {
    enable = true;
    package = pkgs.gitAndTools.gitFull;
    # userName = "John Doe";
    # userEmail = "john.doe@example.com";
    # signing = {
    #   key = "0123456789ABCDEF";
    #   signByDefault = true;
    # };
    aliases = {
      br = "branch";
      ci = "commit";
      co = "checkout";
      st = "status";
      unstage = "reset HEAD --";
      last = "log -1 HEAD";
    };
    extraConfig = {
      color = {
        ui = "auto";
      };
      core = {
        pager = "less -x1,3";
        editor = "vim";
      };
      tag = {
        gpgSign = true;
      };
      init = {
        defaultBranch = "main";
      };
    };
  };

  programs.vscode = {
    enable = true;
    userSettings = {
      "editor.fontSize" = 13;
      "editor.tabSize" = 2;
      "editor.formatOnSave" = true;
      "editor.formatOnPaste" = true;
      "editor.wordWrap" = "on";
      "explorer.confirmDragAndDrop" = false;
      "window.zoomLevel" = 0;
      "files.watcherExclude" = {
        "**/_build/**" = true;
        "**/deps/**" = true;
      };
    };
    extensions = with pkgs.vscode-extensions; [
      bbenoist.nix
      davidanson.vscode-markdownlint
      mhutchie.git-graph
      streetsidesoftware.code-spell-checker
    ];
  };

  programs.home-manager = {
    enable = true;
  };

  services.gpg-agent = {
    enable = true;
    defaultCacheTtl = 1800;
    enableSshSupport = true;
  };
}
