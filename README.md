# Development environment

This guide will help you set your development environment to develop software.

## Virtual machine

Most of us are equipped with physical machine (laptop) running _Windows 10 Pro_. However, since GNU/Linux tools makes our developer life easier, we will need a GNU/Linux operating system. In order to run such a system in our laptops, we will use _VMware Workstation_. It is a software that allows us to run virtual machines within Windows 10 Pro.

### Install VMware Workstation Pro

- Download _VMware Workstation Pro_ from <https://www.vmware.com/products/workstation-pro.html>
- Verify its file integrity

  ```cmd
  certUtil -hashfile <PATH_TO_FILE> SHA256
  ```

- Install the software on your machine
- Buy and active the licence

### Create a development virtual machine

In _VMware Workstation Pro_, click on _Create a New Virtual Machine_ and follow the wizard.

- Select a custom configuration
- Leave the hardware compatibility unchanged
- Select _I will install the operating system later_
- Select _Linux_ for the guest operating system and _Other Linux 5.x 64-bit_ for the version
- Give the virtual machine a name and a location
- Select the number of processors and cores that correspond to your physical machine (e.g., 1 processor, 6 cores)
- Select a comfortable amount of memory (e.g., 8 GB)
- Select _Use bridged networking_
- Leave the I/O controller types unchanged
- Select _NVMe_ disk type
- Select _Create a new virtual disk_
- Choose a comfortable amount of disk size (e.g., 80 GB) and select _Store virtual disk as a single file_
- Choose a file name for the virtual disk
- Click _Finish_ to create the virtual machine
- Click on _Edit virtual machine settings_, go to _Virtual Machine Settings / Options / Advanced / Firmware Type_ and select _UEFI_

That is it!

## Operating system

### Installation

You now have a virtual machine for your development but there is no operating system installed on it.

We are going to install a distribution of GNU/Linux called [_NixOS_](https://nixos.org/manual/nixos/stable/) with a few programs. The following instructions have been tested with NixOS 21.11.

- Download the Graphical live CD (Plasma Desktop, 64bit) of NixOS from the download page <https://nixos.org/download.html#nixos-iso>
- Mount the ISO image in the virtual machine
  - Select _Edit virtual machine settings_ and go to the _CD/DVD (IDE)_ tab
  - Make sure _Connect at power on_ is checked and select _Use ISO image file_
  - Click on _Browse_ and select the file downloaded previously
- Start your virtual machine
- Wait until the desktop environment is started
- In _System Settings > Input Devices > Layouts_, set the layout corresponding to your keyboard
- Launch a console with the `Konsole` program
- Gain root privileges

  ```shell
  sudo -i
  ```

- Check your network configuration

  ```shell
  ip a
  ```

- Partition the drive

  ```shell
  DISK=/dev/nvme0n1
  sgdisk -a1 -n2:34:2047 -t2:EF02 $DISK
  sgdisk -n3:1M:+512M -t3:EF00 $DISK
  sgdisk -n1:0:0 -t1:BF01 $DISK
  ```

- Create the ZFS pool

  ```shell
  zpool create \
    -O mountpoint=none \
    -O compression=lz4 \
    -O xattr=sa \
    -O acltype=posixacl \
    -R /mnt \
    rpool \
    ${DISK}p1
  ```

- Create the ZFS datasets

  ```shell
  # rpool/swap dataset
  zfs create \
    -V 8G \
    -o compression=zle \
    -o logbias=throughput \
    -o sync=always \
    -o primarycache=metadata \
    -o secondarycache=none \
    -o com.sun:auto-snapshot=false \
    rpool/swap
  mkswap -f /dev/zvol/rpool/swap
  swapon /dev/zvol/rpool/swap

  # rpool/root dataset
  zfs create \
    -o mountpoint=legacy \
    -o com.sun:auto-snapshot=true \
    rpool/root
  mount -t zfs rpool/root /mnt

  # rpool/nix dataset
  zfs create \
    -o mountpoint=legacy \
    -o com.sun:auto-snapshot=false \
    rpool/nix
  mkdir /mnt/nix
  mount -t zfs rpool/nix /mnt/nix

  # rpool/home dataset
  zfs create \
    -o mountpoint=legacy \
    -o com.sun:auto-snapshot=true \
    rpool/home
  mkdir /mnt/home
  mount -t zfs rpool/home /mnt/home
  ```

- Set up `/boot` as a non-ZFS partition

  ```shell
  mkfs.vfat ${DISK}p3
  mkdir /mnt/boot
  mount ${DISK}p3 /mnt/boot
  ```

- Generate default configuration files for NixOS

  ```shell
  nixos-generate-config --root /mnt
  ```

- Customize the generated `/mnt/etc/nixos/configuration.nix` file. Note that after installation and the next reboot, this file will be accessible at `/etc/nixos/configuration.nix`. This repository contains a good [`configuration.nix`](./config/nixos/configuration.nix) example. Modify it accordingly:
  - [`networking.hostId`](https://nixos.org/manual/nixos/stable/options.html#opt-networking.hostId)
  - [`networking.hostName`](https://nixos.org/manual/nixos/stable/options.html#opt-networking.hostName)

- Install NixOS

  ```shell
  nixos-install
  ```

- Cleanup

  ```shell
  umount /mnt/{nix,home,boot}
  umount /mnt
  swapoff -a
  zpool export rpool
  ```

- Shut down the machine

  ```shell
  shutdown now
  ```

- Unmount the ISO image in the virtual machine so that the system boots on the freshly installed NixOS.
- Boot the virtual machine and open a terminal
- Create a user account

  ```shell
  useradd -m -G wheel -c '<DISPLAY_NAME>' <USERNAME>
  ```

  Replace `<DISPLAY_NAME>` and `<USERNAME>`, e.g.:

  ```shell
  useradd -m -G wheel -c 'John Doe' jdoe
  ```

- Set a password for the user

  ```shell
  passwd <USERNAME>
  ```

  Replace `<USERNAME>`, e.g.:

  ```shell
  passwd jdoe
  ```

- Reboot and log in as the new user

## User environnement

### Home Manager

Next, we are going to prepare your user environment by installing a series of useful programs with the help of [Home Manager](https://github.com/nix-community/home-manager).

- Create the configuration directory for Home Manager

  ```shell
  mkdir -p ~/.config/nixpkgs/
  ```

- Copy the content from this repository's [`config.nix`](./config/home-manager/config.nix) example into `~/.config/nixpkgs/config.nix`
- Copy the content from this repository's [`home.nix`](./config/home-manager/home.nix) example into `~/.config/nixpkgs/home.nix` and modify it with your personal information (i.e.: `programs.git.userName` and `programs.git.userEmail`)
- Install Home Manager according to the procedure <https://nix-community.github.io/home-manager/index.html#sec-install-standalone>

  In short:

  ```shell
  nix-channel --add https://github.com/nix-community/home-manager/archive/release-21.11.tar.gz home-manager
  nix-channel --update
  ```

  Logout, login again and then:

  ```shell
  nix-shell '<home-manager>' -A install
  ```
